+++
title = "Made of Tungsten"
+++

In this blog I'm talking about the stuff that occupies most of my time currently - things that can be made with Tungsten Fabric (aka Contrail). I'm not necessarily going to write here only about Tungsten Fabric, but also about technology domains it applies to, my research around them and everything I found useful.
