+++
title = "Who I am"
date = "2020-08-30"
author = "Danil Zhigalin"
+++

**Enough bush-beating, I'm starting this blog.** Actually I was thinking for quite a while already. It's going to be rather technical, but let me also share a few words about myself and my background.

I'm from Chelyabinsk, a city in the Ural mountains where people breathe lead and eat steel. Those who survive are rewarded with immunity against nuclear blasts.

![chelyabinsk](/img/chelyabinsk.jpg)

We moved with my mother to Moscow when we decided that we are tempered enough. I finished school with a firm belief that I'm going to become a physicist. I still have it somewhere deep in my heart. That's why I entered to [MIPT University](https://mipt.ru/english/) to study physics. I have some very good memories about those years as well as very special nightmares about sitting an exam and feeling unprepared.

University was also the time when I started my career. I started in a systems integrator, the company which was run by the grads of my university. Routing and switching, it was all exciting for the first couple of years, but then it became obvious that this is only bare bones, which other technology just builds on top of. But after all, MPLS is really cool and I'm happy that I was able to work with big carriers, which were expanding their core networks the hard way, without all those automation and orchestration bells and whistles. You know, doing things the hard way is always important in the beginning. Those years I was [visiting Kazakhstan](https://newsroom.cisco.com/press-release-content?type=webcontent&articleId=3602284) many times for projects and now, looking back, I think I'd like to visit it sometime in future.

![chelyabinsk](/img/kazakh_eagle.jpg)

I graduated and changed to Ericsson at the same time. There I got introduced into the world of Packet Core and mobile services. It was fun travelling around Russia from project to project when 4G was just being rolled out. It felt great being on the cutting edge of technology and seeing LTE sign on your testing phone in the lab, while you know that most of the people in the world don't even know what this stands for but soon it will be everywhere. Back then I also got passionate about Linux and the power that it gives to an engineer compared to the closed world of classical network equipment vendors. Ericsson in those days didn't bother much to hide from the user what their software is running on top of, so working with its products was basically running discrete packages on top of Linux. I also learned that the most fun for me is disentangling complex solutions, reverse engineering and finding out how things really work under the hood.


Then around 2014 things started to go south for Ericsson's packet core and I also felt that I need some new toys to play with, so I changed to an integrator, which was quickly taking the former share of Ericsson in [Packet Core](https://translate.google.com/translate?sl=auto&tl=en&u=https%3A%2F%2Fservernews.ru%2F806190), which promised me to visit all the places I've been to already, but providing solution of a new vendor, which is kind of fun too. And you also learn that things can be done, well, differently than you first learned. I started to pick some python to automate repetitive tasks and that has quickly paid off by improved efficiency. That was also the time I completely fell in love with Vim, which finally caused me to buy a cool and loud mechanical keyboard (my wife doesn't find it so nice though when she hears me typing at night). This period of my life was full of events: I got married, we got a kid, there was a lot to do. That time I was also preparing for CCIE SP exam and also training for a marathon, which is kind of a perfect balance of 2 challenges while being a young father at the same time. I ran the marathon and passed the exam with a month distance.

![medienhaven](/img/sim_cards.jpg)


I'm very fond of Europe and occasionally I learned German at the university, which kind of welcomed me to look for a job there. My wife and me decided, that Düsseldorf would be a nice place to move to, so we moved to Düsseldorf, where I found a position in Dimension Data, the company where I started to work with SDN and NFV and was put in charge of a POC lab where demonstrations/presentations that we made for customers and internal teams were aimed to boost their knowledge of emerging technologies. There I got a good grip on OpenStack and Contrail, as well as DC switching with Juniper network gear. It was a great place to try working with CI/CD pipelines and sharpen the knowledge of ansible and python. It's a very German place, where I found how differently things work in different countries and I feel it was a good place to start in a new country.

![medienhaven](/img/medienhafen.jpg)


Contrail left deep enough footprint on me so that I found a job in Juniper in SRE team, which is the most skilled with all possible solutions where Contrail can fit in. I have to admit it was the most exciting start for me in my career so far and I was and am still learning a lot here. So let's keep learning together and I'll be happy to see your comments.

![tungsten_fabric_army_knife](/img/tungsten_fabric_army_knife.png)

