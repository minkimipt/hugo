+++
title = "Containers and Puppet"
date = "2020-09-09"
author = "Danil Zhigalin"
tags = ["kolla", "containers", "openstack"]
weight = 4
+++

## Where do containers for OpenStack services come from

In previous posts we have seen how TripleO heat templates and Software configuration work. For now it was just enough to know that containers are pulled from registry and used by local container runtime. But where do they come from and how can we change them in case we really want. If you are curious to know the answers, read further.

## Introducing Kolla

You may have heard about [kolla](https://github.com/openstack/kolla) already. 

This project has 2 main streams:
* [Kolla](https://opendev.org/openstack/kolla) - Packaging OpenStack services into container images that adhere to [Microservice Architecture](https://microservices.io/index.html) and to provide a collection of Ansible playbooks to install those containers.
* [Kolla Ansible](https://opendev.org/openstack/kolla-ansible) - Provide a collection of Ansible playbooks to install those containers.

In my opinion Kolla is much easier to start with than with TripleO, but it does not provide baremetal management capabilities and it's more focused on configuring the services and not on managing the underlying OS. Kolla project maintains a hierarchy of j2 templated Docker files and an set of tools to build containers based on those files and to publish them to your registry. It's convenient to use and you can select which packages will be included into containers, which images to base your containers on and provides a lot of other customisation options.

TripleO is using Kolla project to build containers. Kolla is not used in its default configuration, some modifications are made to containers to enable configuration workflows that TripleO supports. There is a project [tripleo-common](https://opendev.org/openstack/tripleo-common), that has all settings and scripts that are used to build container images with kolla in the way, that satisfies TripleO requirements. For more details about building TripleO container images read [tripleo dev documentation 1](https://docs.openstack.org/tripleo-docs/latest/developer/tripleoclient_primer.html) and [tripleo documentation 2](https://docs.openstack.org/project-deploy-guide/tripleo-docs/latest/deployment/3rd_party.html). 

To customize the packages, that kolla puts inside containers, [kolla template override mechanism](https://docs.openstack.org/kolla/latest/admin/image-building.html#dockerfile-customisation) is used. One of the lines of container-images/tripleo_kolla_template_overrides.j2 file from tripleo-common project, that implements this mechanism for this image will have some significance later in this article:

```
{% set base_centos_binary_packages_append = ['openstack-tripleo-common-container-base', 'rsync', 'cronie', 'crudini', 'openstack-selinux', 'ansible', 'python-shade', 'puppet-tripleo', 'python2-kubernetes'] %}
```

In instructs kolla to install specified packages inside container. A package puppet-tripleo has a big list of dependencies, which are basically puppet modules of all services that tripleo can control out of the box. To see all of those puppet modules, run this inside any of tripleo containers:

```
yum history info puppet-tripleo
```

This is part of a base container image, which means that every kolla container built by TripleO will have all puppet modules for all services that TripleO can control.

## Running puppet inside container

There is no holistic way of passing configuration data to the services, that needs to consume this data. Here we'll review how configuration data is passed to core openstack services managed by TripleO.

Configuration that openstack services, managed by tripleo, are using is still generated by puppet. This is the legacy of former TripleO versions, that were purely relying on puppet and didn't used containers. In queens release, puppet runs inside containers and this script `/var/lib/docker-puppet/docker-puppet.py` is used to do in on overcloud servers. This script is started by ansible playbook, that is started by each of the steps, as we have seen in the previous article. This script starts a container based on kolla images described in previous section for each openstack service. Instead of a standard entrypoint `dumb-init --single-child --`, that is configured inside images by kolla, this script is using a special entrypoint script, that is located in `/var/lib/docker-puppet/docker-puppet.sh`. Which puppet manifests in which container this script will run is dictated by `/var/lib/docker-puppet/docker-puppet.json`. This json file is a list of dictionaries, each element of this has the name of the image to run and the basic puppet manifests to be executed in `include` statements. All the input data for those manifests is taken from hieradata, that is located on the host and that is created by allNodesConfig resource, that we talked about in the first blog post.

As an example we can take an element of:

  {
    "config_image": "100.86.148.101:8787/rhosp13/openstack-nova-placement-api:13.0-119",
    "step_config": "include tripleo::profile::base::nova::placement\n\ninclude ::tripleo::profile::base::database::mysql::client",
    "config_volume": "nova_placement",
    "puppet_tags": "nova_config"
  },

What we see here is step_config, which includes 2 `include` statements:

```
include tripleo::profile::base::nova::placement
include ::tripleo::profile::base::database::mysql::client
```

When docker-puppet.py is started, it runs a container in a following way:

```
        dcmd = ['/usr/bin/docker', 'run',
                '--user', 'root',
                '--name', uname,
                '--env', 'PUPPET_TAGS=%s' % puppet_tags,
                '--env', 'NAME=%s' % config_volume,
                '--env', 'HOSTNAME=%s' % short_hostname(),
                '--env', 'NO_ARCHIVE=%s' % os.environ.get('NO_ARCHIVE', ''),
                '--env', 'STEP=%s' % os.environ.get('STEP', '6'),
                '--volume', '/etc/localtime:/etc/localtime:ro',
                # Manifest with the `include` statements mentioned above is written to a tmp file, which is passed into container as /etc/config.pp and is being executed by puppet
                '--volume', '%s:/etc/config.pp:ro,z' % tmp_man.name,
                '--volume', '/etc/puppet/:/tmp/puppet-etc/:ro,z',
                '--volume', '/usr/share/openstack-puppet/modules/:/usr/share/openstack-puppet/modules/:ro',
                '--volume', '%s:/var/lib/config-data/:z' % os.environ.get('CONFIG_VOLUME_PREFIX', '/var/lib/config-data'),
                '--volume', 'tripleo_logs:/var/log/tripleo/',
                # Syslog socket for puppet logs
                '--volume', '/dev/log:/dev/log',
                # OpenSSL trusted CA injection
                '--volume', '/etc/pki/ca-trust/extracted:/etc/pki/ca-trust/extracted:ro',
                '--volume', '/etc/pki/tls/certs/ca-bundle.crt:/etc/pki/tls/certs/ca-bundle.crt:ro',
                '--volume', '/etc/pki/tls/certs/ca-bundle.trust.crt:/etc/pki/tls/certs/ca-bundle.trust.crt:ro',
                '--volume', '/etc/pki/tls/cert.pem:/etc/pki/tls/cert.pem:ro',
                # facter caching
                '--volume', '/var/lib/container-puppet/puppetlabs/facter.conf:/etc/puppetlabs/facter/facter.conf:ro',
                '--volume', '/var/lib/container-puppet/puppetlabs/:/opt/puppetlabs/:ro',
                # script injection
                '--volume', '%s:%s:z' % (sh_script, sh_script) 
                # entrypoint shell script is also created in this docker-puppet.py
                '--entrypoint', '/var/lib/docker-puppet/docker-puppet.sh'
        ]
```


Important part of the entrypoint script is the execution of puppet manifest, that will generate all configuration files:

```
/usr/bin/puppet apply --summarize --detailed-exitcodes --color=false --logdest syslog --logdest console --modulepath=/etc/puppet/modules:/usr/share/openstack-puppet/modules $TAGS /etc/config.pp
```

## Passing configuration parameters inside containers

Now that we know, how configuration files are created, we need to understand, how to control the way, how they are being created. And in which places of THT we can inject this information, so that it becomes available to puppet inside containers.

Taking further the example service nova_placement, that I've mentioned above, we can find a place, where configuration parameters are injected in THT:

If we inspect the file puppet/services/nova-placement.yaml, we will see in outputs:

```
  role_data:
    description: Role data for the Nova Placement API service.
    value:
      service_name: nova_placement
      monitoring_subscription: {get_param: MonitoringSubscriptionNovaPlacement}
      config_settings:
        map_merge:
        - get_attr: [NovaBase, role_data, config_settings]
        - get_attr: [ApacheServiceBase, role_data, config_settings]
        - tripleo.nova_placement.firewall_rules:
            '138 nova_placement':
              dport:
                - 8778
                - 13778
          nova::keystone::authtoken::project_name: 'service'
          nova::keystone::authtoken::password: {get_param: NovaPassword}
```

These values that are passed into `config_settings` are used exactly for this. If we cross-check this in the manifest inside any containers, that was created from the image 100.86.148.101:8787/rhosp13/openstack-nova-placement-api:13.0-119, we will see familiar variables there:


```
[root@overcloudn5w-ctrl-1 heat-admin]# docker exec -ti nova_placement bash
()[root@overcloudn5w-ctrl-1 modules]# grep -r "nova::keystone::authtoken::project_name" ./
./nova/manifests/api.pp:    $atnr = $::nova::keystone::authtoken::project_name
```

This varialbe is indeed used inside the manifest and we can change it in TripleO if we went to manipulate which project_name nova-placement should use to authenticate with keystone. To understand what this module is doint we need to know puppet better, but here are a few tips that will help you navigate the module code:

Usually sections of the module that control which settings are passed inside the config file look like this:

```
  nova_config {
    'wsgi/api_paste_config':                       value => $api_paste_config;
    'DEFAULT/enabled_apis':                        value => join($enabled_apis_real, ',');
    'DEFAULT/osapi_compute_listen':                value => $api_bind_address;
```

this gets translated into 

```
[DEFAULT]
enabled_apis = join($enabled_apis_real, ',')
osapi_compute_listen =  $api_bind_address
[wsgi]
api_paste_config = $api_paste_config
```

how these variables are set inside the module it's easy to fine using grep:

```
  $api_paste_config                            = 'api-paste.ini',
  $enabled_apis_real = $enabled_apis
  $api_bind_address                            = '0.0.0.0',
```
